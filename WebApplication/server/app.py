from flask import Flask, request, jsonify, render_template

import util

app = Flask(__name__)

@app.route("/")
def hello():
    ctx = dict(locations=util.get_location_names())
    return render_template("index.html", **ctx)

@app.route('/get_location_names')
def get_location_names():
    response = jsonify({
        'location': util.get_location_names()
    })
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/predict_home_price', methods=['POST'])
def predict_home_price():
    total_sqft = float(request.form['area'])
    location = request.form['location']
    bhk = int(request.form['bhk'])
    bath = int(request.form['bathroom'])

    response = jsonify({
        'estimated_price': util.get_estimated_price(location, total_sqft, bhk, bath)
    })

    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

if __name__ == "__main__":
    app.run(debug=True)